package HomeWork;

/**
 * Created by Kamil on 24.02.15.
 */
public class H2_LListStack<T> {
    private Elem<T> head;

    public H2_LListStack() {
        head = null;
    }

    class Elem<T>{
        private T val = null;
        Elem<T> next = null;
    }

    void push(T x){
        Elem<T> p = new Elem<T>();
        p.val = x;
        p.next = head;
        head = p;
    }

    boolean isEmpty(){
        return head == null;
    }

    T pop(){
        T a = head.val;
        head = head.next;
        return a;
    }

}
