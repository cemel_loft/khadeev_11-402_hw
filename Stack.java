package HomeWork;

import java.util.ArrayList;

/**
 * Created by Kamil on 13.02.15.
 */
class Stack<T>{
    private int n;
    private int rIndex;
    ArrayList<T> arr;

    public Stack(int l){
        n = l;
        arr = new ArrayList<T>();
        for (int i=0;i<n;i++)
            arr.add(null);
    }

    public T pop() throws UnderFlow {//взять
        if(rIndex != 0){
            rIndex --;
            return arr.get(n);
        }else{
            throw new UnderFlow();
        }
    }
    public void push (T x) throws OverFlow {
        if (rIndex < n) {
            arr.set(rIndex, x);
            rIndex++;
        }else{
            throw new OverFlow();
        }
    }
    public boolean isEmpty(){
        return rIndex==0;
    }

}
