package HomeWork;

import ClassWork.LListStake;
import HomeWork.H2_LListStack;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Kamil on 24.02.15.
 */
public class StackkTest {
    @Test
    public void pushToEmpty() throws OverFlow {
        Stackk<Integer> s = new Stackk<Integer>(1);
        s.push(1);
        assertFalse(s.isEmpty());
    }
    @Test
    public void popToEmpty() throws UnderFlow {
        LListStake<Integer> d = new LListStake<Integer>();
        int x = 1;
        d.push(x);
        assertTrue(d.pop() == x);
    }
    @Test
    public void EmtyToEmty(){
        LListStake<Integer> s = new LListStake<Integer>();
        assertTrue(s.isEmpty());
    }
    @Test(expected = OverFlow.class)
    public void OverFlow () throws OverFlow {
        Stackk<Integer> s = new Stackk<Integer>(1);
        s.push(1);
        s.push(1);
        s.push(1);
    }
}
