package HomeWork.MyIterator;

import java.util.ListIterator;
import java.util.NoSuchElementException;

/**
 * Created by Kamil on 06.03.15.
 */

public class MyIterator<T> implements ListIterator<T> {

    private Elem<T> cur;
    private Elem<T> prev;
    private int index;
    private boolean lastOpWasNext;
    private boolean isLastOpWasPrev;

    public MyIterator(Elem<T> head){
        cur = head;
        prev = null;
        index = 0;
    }

    @Override
    public boolean hasNext() {
        return cur != null;
    }

    @Override
    public T next() throws NoSuchElementException{
        if(!hasNext()){
            throw new NoSuchElementException();
        }
        T elem = cur.elem;
        prev = cur;
        cur = cur.next;
        index++;
        lastOpWasNext = true;
        isLastOpWasPrev = false;
        return prev.elem;
    }

    @Override
    public boolean hasPrevious() {
        return prev != null;
    }

    @Override
    public T previous() {
        if(!this.hasPrevious())
            throw new NoSuchElementException();
        cur = prev;
        prev = prev.prev;
        index --;
        lastOpWasNext = false;
        lastOpWasNext = true;
        return cur.elem;
    }

    @Override
    public int nextIndex() {
        return index;
    }

    @Override
    public int previousIndex() {
        return index - 1;
    }

    @Override
    public void remove() throws UnsupportedOperationException{
        throw new UnsupportedOperationException();
    }

    @Override
    public void set(T elem) throws IllegalStateException{
        Elem<T> del = null;
        if(lastOpWasNext){
            del = prev;
        }
        if (isLastOpWasPrev){
            del = cur;
        }
        if(del == null){
            throw new IllegalStateException();
        }

    }

    @Override
    public void add(T elem) throws UnsupportedOperationException{
        throw new UnsupportedOperationException();
    }
}
