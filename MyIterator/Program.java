package HomeWork.MyIterator;

/**
 * Created by Kamil on 16.03.15.
 */
import java.util.List;
import java.util.ListIterator;

public class Program {
    static <T> Elem<T> add(Elem<T> head, T elem){
        Elem<T> p = new Elem<T>();
        p.elem = elem;
        p.prev = null;
        p.next = head;
        return p;

    }

    public static void main(String[] args) {
        Elem<Integer> head = null;
        head = add(head, 5);
        head = add(head, 4);
        head = add(head, 3);
        head = add(head, 2);
        head = add(head, 1);

        Elem<Integer> p = head;
        while (p != null){
            System.out.print(p.elem + " ");
            p = p.next;
        }

        System.out.println();

        List<Integer> list = new MyList<Integer>(head);
        ListIterator<Integer> it = list.listIterator();

//        System.out.println(it.next());
//        System.out.println(it.previous());
        for(int i = 0; i < 5; i++){
            System.out.println(it.next());
        }
//        System.out.println(it.next());
//        System.out.println(it.previous());
    }
}
