package HomeWork.MyIterator;

import java.util.AbstractSequentialList;
import java.util.ListIterator;

/**
 * Created by Kamil on 16.03.15.
 */

class Elem<T>{
    T elem;
    Elem<T> next;
    Elem<T> prev;
    Elem<T> current;
    int index;
}

public class MyList<T> extends AbstractSequentialList<T> {
    public int size;
    private Elem<T> head = new Elem<T>();
    public MyList(Elem<T> head){
        this.head = head;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        MyIterator<T> it = new MyIterator<T>(head);
        for (int i = 0; i < index; i++) {
            it.next();
        }
        return it;
    }

    @Override
    public int size() {
    return this.size;
    }
}

