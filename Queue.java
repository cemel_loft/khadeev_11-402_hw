package HomeWork;

import java.util.ArrayList;

/**
 * Created by Kamil on 17.02.15.
 */
class Queue<T> {
    private int n;
    private int rIndex;
    private int lIndex;
    private int k = 0;
    ArrayList<T> arr;

    public Queue(int l){
        n = l;
        arr = new ArrayList<T>();
        for (int i=0;i<n;i++)
            arr.add(null);
        k = 0;
    }

    public T pop() throws UnderFlow {//взять//deq
        if(rIndex != 0&& k!=0){
            T x = arr.get(lIndex);
            lIndex = (rIndex + 1)%n;
            k--;
            return x;
        }else{
            throw new UnderFlow();
        }
    }
    public void push (T x) throws OverFlow {//que
        if (rIndex < n && k!=n) {
            arr.set(rIndex, x);
            rIndex = (rIndex + 1)%n;
            k++;
        }else{
            throw new OverFlow();
        }
    }
    public boolean isEmpty(){
        return rIndex==0;
    }

}
