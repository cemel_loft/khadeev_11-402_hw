package semestrovaya;

/**
 * Created by Kamil on 26.03.15.
 */import java.io.*;


public class CircleList {
    private Elem head;

    public CircleList() {
    }

    public CircleList(String filename) throws IOException {
        try {
            BufferedReader buferReader = new BufferedReader(new FileReader(filename));
            String str;
            String[] array;
            if ((str = buferReader.readLine()) != null) {
                array = str.split(" ");
                if (array.length == 1) {
                    head = new Elem(null, new Participant(array[0], array[0]));
                    head.setNext(head);
                }
            }
            Elem q = head;
            while ((str = buferReader.readLine()) != null) {
                array = str.split(" ");
                if (array.length != 1) break;
                Elem p = new Elem(head, new Participant(array[0],array[0]));
                q.setNext(p);
                q = p;
//
            }
        } catch (IOException e) {
        }
    }


    void show() throws NullPointerException,IOException {
        Elem p = head;
        Elem q = head.getNext();
        System.out.println(head.getPerson().toString());
        while (!q.equals(p)) {
            System.out.println(q.getPerson().toString());
            q = q.getNext();


        }
    }


    void delete(String name) throws IOException {
        Elem p = head.getNext();
        Elem q = head;
        while (p != head) {
            if (p.getPerson().getName().equals(name)) {
                q.setNext(q.getNext().getNext());
                return;
            }
            p = p.getNext();
            q = q.getNext();
        }
        if (p.getPerson().getName().equals(name)) {
            q.setNext(null);
            head = head.getNext();
            q.setNext(head);
        }
    }


    Participant last(int n, int k) throws IOException {
        Elem p = head.getNext();
        Elem q = head;

        int res = 0;
        for (int i=1; i<=n; ++i)
            res = (res + k) % i;
        while ((res+1) != 1){
            p = p.getNext();
            q = q.getNext();
            res--;
        }
        return p.getPerson();
    }


}
