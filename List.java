package HomeWork;

/**
 * Created by Kamil on 02.03.15.
 */
public class List<T> {
    class Node{
        T elem;
        Node next;
        Node prev;
    }
    private Node head;
    private Node last;
    private Node tail;

    public void addFirst(T x){
        Node p = new Node();
        p.elem = x;
        p.prev = null;
        p.next = head;
        if (head == null) {
            last = p;
        }else{
            head.prev = p;
        }
        head = p;
    }

    public void addEnd(T x){
        Node p = new Node();
        p.elem = x;
        p.next = null;
        p.prev = last;
        if(head == null){
            head = p;
        }else{
            last.next = p;
        }
        last = p;
    }

    public T deleteFirst(){
        Node first = head;
        head = head.next;
        return first.elem;
    }
    public T deleteEnd(){
        Node end = last;
        last = last.prev;
        return end.elem;
    }

    public static void main(String[] args) {

    }
}