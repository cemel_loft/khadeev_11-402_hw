package semestrovaya;

public class Elem {
    private Elem next;
    private Participant person;

    public Elem getNext() {
        return next;
    }

    public void setNext(Elem next) {
        this.next = next;
    }

    public Participant getPerson() {
        return person;
    }

    public void setPerson(Participant person) {
        this.person = person;
    }

    public Elem(Elem next, Participant person) {
        this.next = next;
        this.person = person;
    }

}