package semestrovaya;

/**
 * Created by Kamil on 26.03.15.
 */
public class Participant {
    private String name;
    private String pol;

    public String toString() {
        return  this.getName();
    }

    public Participant() {

    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPol() {
        return pol;
    }

    public void setPol(String pol) {
        this.pol = pol;
    }

    public Participant(String name, String pol) {
        this.name = name;
        this.pol = pol;
    }


}